import 'package:flutter/material.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:testinvest/widgets/appBarMenuWidget.dart';
import 'package:testinvest/widgets/buttonWidget.dart';
import 'package:testinvest/widgets/tagButtonWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


    @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarMenuWidget(context),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: MediaQuery.of(context).size.width > 600
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _leftSide(),
                      _rightSide(),
                    ],
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _leftSide(),
                      _rightSide(),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
  

  Widget _textColumn(String text, String bottomText,
      {bool rightField = false}) {
    return Container(
      padding: !rightField
          ? EdgeInsets.only(
              top: 10.0,
              bottom: 10.0,
            )
          : EdgeInsets.all(10.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.normal,
              color: Colors.grey[500],
              fontSize: 23),
        ),
        Text(
          bottomText,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        )
      ]),
    );
  }

  Widget _leftSide() {
    var screenSize = MediaQuery.of(context).size;
    double _width = 500.0;
    if (screenSize.width < 600)
      _width = screenSize.width / 1;
    else if (screenSize.width < 1000) _width = screenSize.width / 2;

    return Container(
      width: _width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.all(20.0),
              color: Colors.green[100],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "3 days Left",
                    style: TextStyle(fontSize: 25),
                  ),
                  Text("Estonia", style: TextStyle(fontSize: 25))
                ],
              )),
          SizedBox(
            height: 30.0,
          ),
          Container(
            padding: EdgeInsets.all(20.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Fawn Greek Pistachio Orchard",
                  style: TextStyle(fontSize: 30, color: Colors.green[900])),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Aasmae, Estonia",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.grey[500],
                    fontSize: 23),
              ),
              SizedBox(
                height: 30.0,
              ),
              Table(
                border: TableBorder(
                    verticalInside: BorderSide(
                        width: 1,
                        color: Color(0xFFE0E0E0),
                        style: BorderStyle.solid),
                    horizontalInside: BorderSide(
                        width: 1,
                        color: Color(0xFFE0E0E0),
                        style: BorderStyle.solid)),
                children: [
                  TableRow(children: [
                    _textColumn('Yield Rate', '5%'),
                    _textColumn('Risk Rate', 'B', rightField: true),
                  ]),
                  TableRow(children: [
                    _textColumn('Price', '€ 100, 00'),
                    _textColumn('Rased', '€ 65, 000', rightField: true),
                  ]),
                ],
              ),
              SizedBox(height: 40.0),
              Text(
                "Commited",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.grey[500],
                    fontSize: 23),
              ),
              SizedBox(height: 20.0),
              Text(
                "64.90 %",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.green[900],
                    fontSize: 23),
              ),
              SizedBox(height: 10.0),
              LinearPercentIndicator(
                //width: 170.0,
                animation: true,
                animationDuration: 1000,
                lineHeight: 10.0,
                padding: EdgeInsets.all(0),
                percent: 0.649,
                // center: Text("20.0%"),
                linearStrokeCap: LinearStrokeCap.butt,
                progressColor: Colors.green[900],
              ),
              SizedBox(height: 40.0),
              Align(
                alignment: Alignment.center,
                child: Wrap(
                  children: [
                    ButtonWidget("Invest", color: Color(0xff1b5e20), textColor: Colors.white, callback: () => print("test inest"),),
                    ButtonWidget("More", color: Colors.transparent, textColor: Color(0xff1b5e20), callback: () => print("test more")),
                  ],
                ),
              )
            ]),
          ),
        ],
      ),
    );
  }

  Widget _rightSide() {
    var screenSize = MediaQuery.of(context).size;
    double _width = 500.0;
    if (screenSize.width < 600)
      _width = screenSize.width / 1;
    else if (screenSize.width < 1000) _width = screenSize.width / 2;

    return Container(
        width: _width,
        child: Column(
          children: [
            Container(
                width: 500,
                height: 250,
                child: Image(
                  image: AssetImage('lib/assets/Semis_dans_cv_sud_est.jpg'),
                  fit: BoxFit.cover,
                )),
            Container(
              padding: EdgeInsets.only(top: 20.0),
              height: 400, //MediaQuery.of(context).size.height - 380,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  DottedLine(
                    dashLength: 10,
                    dashGapLength: 4,
                    direction: Axis.vertical,
                    lineLength: double.infinity,
                    lineThickness: 2.0,
                    dashColor: Color(0xFFE0E0E0),
                    dashRadius: 0.0,
                    dashGapColor: Colors.transparent,
                    dashGapRadius: 0.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Aasmae, Estonia",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.grey[500],
                              fontSize: 23),
                        ),

                        SizedBox(height: 20),
                        TagButtonWidget(
                            "Agriculture", icon: Icons.auto_awesome_mosaic_sharp),

                        //SizedBox(height: 20),

                        Image(
                          width: screenSize.width < 750
                              ? MediaQuery.of(context).size.width / 2.5
                              : 350,
                          image: AssetImage('lib/assets/estoniamap.png'),
                          fit: BoxFit.fill,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }


}
