import 'package:flutter/material.dart';

typedef void StringCallback();

class ButtonWidget extends StatelessWidget {

  final StringCallback? callback;

  String text;
  Color color;
  Color textColor;
  ButtonWidget(this.text, {this.callback, this.color = Colors.green, this.textColor = Colors.white });
  
  // This widget is the root of your application.

  
  @override
  Widget build(BuildContext context) {
    return button();
  }

  Widget button() {
    return Container(
      color: color,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: callback,
          child: Container(
              width: 230,
              padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
              child: Text(
                text,
                style: TextStyle(
                  color: textColor,
                  fontSize: 18,
                ),
                textAlign: TextAlign.center,
              )),
        ),
      ),
    );
  }

}
