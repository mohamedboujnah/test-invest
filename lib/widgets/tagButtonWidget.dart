import 'package:flutter/material.dart';

typedef void StringCallback();

class TagButtonWidget extends StatelessWidget {

  final StringCallback? callback;

  String text;
  IconData? icon;
  TagButtonWidget(this.text, {this.callback, this.icon });
   
  @override
  Widget build(BuildContext context) {
    return tagButton();
  }

  Widget tagButton() {
    return Container(
        //color: Colors.orangeAccent,

        //width: 100,
        decoration: new BoxDecoration(
            color: Colors.orange[100],
            borderRadius: new BorderRadius.all(Radius.circular(20.0))),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(icon, color: Colors.lightGreen[800]),
              SizedBox(
                width: 10.0,
              ),
              Text(
                text,
                style: TextStyle(color: Colors.lightGreen[800], fontSize: 18),
              )
            ],
          ),
        ));
  }

}
