import 'package:flutter/material.dart';

  appBarMenuWidget(BuildContext context) {
    
    var screenSize = MediaQuery.of(context).size;

    return PreferredSize(
      preferredSize: Size(screenSize.width, 1000),
      child: Container(
        color: Color(0XFFeeeeee),
        child: Padding(
          padding: EdgeInsets.all(20),
          child: screenSize.width > 760
              ? Row(
                  children: [
                    Text('Logo'),
                    SizedBox(width: 50.0),
                    Expanded(
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          _itemMenuText(
                            context,
                              text: "Invest",
                              dropDown: true,
                              positionLeft: true),
                          _itemMenuText(context, text: "Portfolio", positionLeft: true),
                          _itemMenuText(context, text: "Blog", positionLeft: true),
                          _itemMenuText(
                            context,
                              text: "Learning Center",
                              dropDown: true,
                              positionLeft: true),
                        ],
                      ),
                    ),
                    _itemMenuText(
                      context,
                        text: "€ 1500, 00", fontWeight: FontWeight.bold),
                    _itemMenuText(context, icon: Icons.person_pin),
                    _itemMenuText(context, icon: Icons.notifications),
                    _itemMenuText(context, text: "EN | €", fontWeight: FontWeight.bold),
                  ],
                )
              : Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.menu,
                      color: Colors.green,
                    ),
                  ],
                ),
        ),
      ),
    );

  }


  
  Widget _itemMenuText(
    BuildContext context,
      {String? text,
      IconData? icon,
      bool dropDown: false,
      bool positionLeft = false,
      FontWeight fontWeight: FontWeight.normal}) {
    var screenSize = MediaQuery.of(context).size;
    return Wrap(
      children: [
        InkWell(
          onTap: () {},
          child: text != null
              ? Row(
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                          color: Colors.green, fontWeight: fontWeight),
                    ),
                    dropDown
                        ? Icon(Icons.arrow_drop_down, color: Colors.green)
                        : Container()
                  ],
                )
              : Icon(
                  icon,
                  color: Colors.green,
                  size: 25.0,
                ),
        ),
        SizedBox(width: screenSize.width / (positionLeft ? 20 : 50))
      ],
    );
  }

